@if (count($replies) >= 1)
    <div class="row justify-content-end mr-3" style="margin-top: -2em">
        <div class="col-md-11">
            <div class="card">
                <div class="card-body">
                    @foreach ($replies as $reply)
                        <div class="media mb-3">
                            <img width="40" height="40" src="{{ $reply->user->avatar() }}" class="mr-3 rounded-circle" style="object-fit: cover; object-position: center;" alt="...">
                            <div class="media-body">
                                <a href="#" class="mt-0 d-block">{{ $reply->user->name }}</a>
                                <div class="text-secondary">
                                    {!! nl2br($reply->body) !!}
                                </div>
                                <small class="text-secondary d-flex justify-content-between">
                                    <div>
                                        Replied {{ $reply->created_at->diffForHumans() }}
                                        @can('update', $reply)
                                            &middot; <a href="{{ route('replies.edit', [$thread, $reply]) }}">Edit</a>
                                        @endcan
                                    </div>
                                    @can('update', $thread)
                                        <div>
                                            &middot; <a href="{{ route('answer.create', $reply) }}" class="text-success"
                                             onclick="event.preventDefault(); document.getElementById('mark-as-answer-{{ $reply->hash }}').submit();"
                                            >Mark as answer</a>
                                        </div>

                                        <form action="{{ route('answer.create', $reply) }}" method="post" id="mark-as-answer-{{ $reply->hash }}" style="display: none;">
                                            @csrf
                                            @method("PATCH")
                                        </form>
                                    @endcan
                                </small>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
