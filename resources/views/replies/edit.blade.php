@extends('layouts.app')
@section('title', $thread->title)
@section('content')
    <div class="card">
        <div class="card-header">{{ $thread->title }}</div>
        <div class="card-body">
            <div class="media">
                <img width="40" height="40" src="{{ $thread->user->avatar() }}" class="mr-3 rounded-circle" style="object-fit: cover; object-position: center;" alt="...">
                <div class="media-body">
                    <a href="#" class="mt-0 d-block">{{ $thread->user->name }}</a>
                    <div class="text-secondary">
                        {!! nl2br($thread->body) !!}
                    </div>
                    <small class="text-secondary">
                        Posted {{ $thread->created_at->diffForHumans() }} &middot; <a href="#" class="text-secondary">{{ $thread->tag->name }}</a>

                        @if ($thread->user_id == Auth::id())
                            &middot; <a href="{{ route('threads.edit', [$thread->tag, $thread]) }}">Edit</a>
                        @endif
                    </small>
                </div>
            </div>
        </div>
    </div>
    <div class="media mt-3">
        <img width="40" height="40" src="{{ auth()->user()->avatar() }}" class="mr-3 rounded-circle" style="object-fit: cover; object-position: center;" alt="...">
        <div class="media-body">
            <div class="mb-1 text-secondary">
                <strong>{{ auth()->user()->name }}</strong>
            </div>
            <div class="text-secondary">
                <form action="{{ route('replies.edit', [$thread, $reply]) }}" method="post">
                    @method("PATCH")
                    @csrf
                    <div class="form-group">
                        <textarea name="body" id="body" rows="5" class="form-control" style="resize: none;" placeholder="Submit a comment...">{{ old('body') ?? $reply->body }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
