<div class="list-group mb-2">
    <a href="{{ route('threads.create') }}" class="list-group-item list-group-item-action{{ Request::is('create-new-thread') ? ' active' : '' }}">
        New thread
    </a>
    <a href="{{ route('threads') }}" class="list-group-item list-group-item-action{{ Request::is('forum') ? ' active' : '' }}">
        Latest threads
    </a>
    <a href="/forum/filter?by=popular" class="list-group-item list-group-item-action{{ Request::fullUrl() == url("forum/filter?by=popular") ? ' active' : '' }}">
        Popular
    </a>
    @auth
    <a href="/forum/filter?by=me" class="list-group-item list-group-item-action{{ Request::fullUrl() == url("forum/filter?by=me") ? ' active' : '' }}">
        My threads
    </a>
    @endauth
    <a href="/forum/filter?by=answered" class="list-group-item list-group-item-action{{ Request::fullUrl() == url("forum/filter?by=answered") ? ' active' : '' }}">
        Answered
    </a>
    <a href="/forum/filter?by=unanswered" class="list-group-item list-group-item-action{{ Request::fullUrl() == url("forum/filter?by=unanswered") ? ' active' : '' }}">
        Not yet replies
    </a>
</div>
<tags></tags>
