<?php

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');

/**
 * Account
 */
Route::get('account/edit', 'AccountController@edit')->name('account.edit');
Route::patch('account/edit', 'AccountController@update');

/**
 * Filtering
 */
Route::get('forum/filter', 'Forum\FilterController@index');

/**
 * Search threads
 */
Route::get('forum/search', 'Forum\SearchController@index')->name('threads.search');

/**
 * Tag
 */
Route::get('forum/tags', 'Forum\TagController@index');
Route::get('forum/{tag}', 'Forum\TagController@show')->name('tags.show');

/**
 * Thread
 */
Route::get('forum', 'Forum\ThreadController@index')->name('threads');
Route::get('create-new-thread', 'Forum\ThreadController@create')->name('threads.create');
Route::post('create-new-thread', 'Forum\ThreadController@store');
Route::get('forum/{tag}/{thread}', 'Forum\ThreadController@show')->name('threads.show');
Route::get('edit-my-thread-on/{tag}/{thread}', 'Forum\ThreadController@edit')->name('threads.edit');
Route::patch('edit-my-thread-on/{tag}/{thread}', 'Forum\ThreadController@update');
Route::delete('delete-my-thread/{thread}', 'Forum\ThreadController@destroy')->name('threads.delete');

/**
 * Reply
 */
Route::post('reply-on-the-thread/{thread}', 'Forum\ReplyController@store')->name('replies.create');
Route::get('edit-reply-on-the-thread/{thread}/{reply}', 'Forum\ReplyController@edit')->name('replies.edit');
Route::patch('edit-reply-on-the-thread/{thread}/{reply}', 'Forum\ReplyController@update');

/**
 * Answer
 */
Route::patch('mark-as-answer-at-reply/{reply}', 'Forum\AnswerController@store')->name('answer.create');

Route::get('{user}', 'UserController@show')->name('users.show');
