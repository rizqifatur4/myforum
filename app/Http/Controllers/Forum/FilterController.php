<?php

namespace App\Http\Controllers\Forum;

use App\Models\Forum\Thread;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilterController extends Controller
{
    public function index()
    {
        $query = request('by');
        
        switch ($query) {
            case $query == "popular":
                $threads = Thread::whereHas('replies')->orderBy('replies_count', 'DESC')->paginate(10);
                break;
            case $query == "me":
                if (auth()->check()) {
                    $threads = auth()->user()->threads()->latest()->paginate(10);
                } else {
                    return redirect('/login');
                }
                break;
            case $query == "answered":
                $threads = Thread::whereNotNull('reply_id')->latest()->paginate(10);
                break;
            case $query == "unanswered":
                $threads = Thread::doesntHave('replies')->latest()->paginate(10);
                break;
            
            default:
                return redirect('/forum');
                break;
        }
        return view('threads.index', compact('threads', 'query'));
    }
}
