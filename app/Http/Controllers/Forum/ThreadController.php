<?php

namespace App\Http\Controllers\Forum;

use App\Http\Controllers\Controller;
use App\Http\Requests\Forum\ThreadRequest;
use App\Models\Forum\{Tag, Thread};
use Illuminate\Http\Request;

class ThreadController extends Controller
{
    //agar user yang tidak login tidak bisa mengakses
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    public function index()
    {
        //melimit thread yang ditampilkan
        $threads = Thread::latest()->paginate(10);
        return view('threads.index', compact('threads'));
    }

    public function create()
    {
        $thread = new Thread;
        return view('threads.create', compact('thread'));
    }

    public function store(ThreadRequest $request)
    {
        //agar link thread tidak ada yang sama, walaupun threadnya sama
        $slug = \Str::slug(request('title')) . "-" . \Str::random(6);
        $thread = auth()->user()->threads()->create($this->requestThreadForm($slug));
        return redirect()->route('threads.show', [$thread->tag, $thread]);
    }

    public function show(Tag $tag, Thread $thread)
    {
        $replies = $thread->replies()->orderBy('created_at', 'ASC')->get();
        return view('threads.show', compact('tag', 'thread', 'replies'));
    }

    public function edit(Tag $tag, Thread $thread)
    {
        return view('threads.edit', compact('thread'));
    }

    public function update(ThreadRequest $request, Tag $tag, Thread $thread)
    {
        $this->authorize('update', $thread);
        $slug = \Str::slug(request('title')) . "-" . \Str::random(6);
        $thread->update($this->requestThreadForm($slug));
        return redirect()->route('threads.show', [ $thread->tag, $thread ]);
    }

    protected function requestThreadForm($slug)
    {
        return [
            'title' => request('title'),
            'slug' => $slug,
            'body' => request('body'),
            'tag_id' => request('tag'),
        ];
    }

    public function destroy(Thread $thread)
    {

        $this->authorize('update', $thread);

        $thread->delete();

        return redirect()->route('threads');
    }
}
