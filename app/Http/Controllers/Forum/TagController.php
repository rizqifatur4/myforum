<?php

namespace App\Http\Controllers\Forum;

use App\Models\Forum\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
    public function index()
    {
        return Tag::get();
    }

    public function show(Tag $tag)
    {
        $threads = $tag->threads()->latest()->paginate(10);
        return view('threads.index', compact('threads', 'tag'));
    }
}
