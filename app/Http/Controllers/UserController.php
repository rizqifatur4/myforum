<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show($user)
    {
        $user = User::withCount('threads')->whereHash($user)->orWhere('username', $user)->firstOrFail();
        $threads = $user->threads()->latest()->paginate(10);
        return view('users.show', compact('threads', 'user'));
    }
}
