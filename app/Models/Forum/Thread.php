<?php

namespace App\Models\Forum;
use App\User;
use App\Traits\HasManyReply;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{

    use Searchable, HasManyReply;

    //agar user tidak bisa input selain yang disuruh
    protected $fillable = ['title', 'slug', 'body', 'tag_id'];
    //agar tidak menduplikat query
    protected $with = ['tag', 'user'];

    protected $withCount = ['replies'];

    public function searchableAs()
    {
        return 'threads';
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }

    public function answer()
    {
        return $this->belongsTo(Reply::class, 'reply_id');
    }
}
